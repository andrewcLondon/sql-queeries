const myQuestions = [
    {
        question: "<b>Where is the facility of S4 located?</b>",
        answers: {
            a: '10-15 miles South of Area 51',
            b: 'Texas',
            c: 'Amazon S4 warehouse'
        },
        correctAnswer: 'a'
    },
    {
        question: "<b>If the earth is truly a disc, where does the water go when it falls off the edge?</b>",
        answers: {
            a: 'Falls downward in space',
            b: 'Into another dimension',
            c: 'Onto the other side of the disc'
        },
        correctAnswer: 'c'
    },
    {
        question: "<b>Bob Lazar’s main reason for telling his story and spilling American secrets is which of the following:</b>",
        answers:
            {
                a: 'He thought the American people should know how cool Alien technology is',
                b: 'He wanted to try and make lots of money through publicity',
                c: 'His wife cheated on him which lead him to tell people where he worked out of safety concerns for his life'
            },
        correctAnswer: 'c'
    },

    {
        question: "<b>Did Jeffrey Epstein kill himself?</b>",
        answers:
            {
                a: 'Yes',
                b: 'No – He was murdered',
                c: 'Prince Andrew killed him'
            },
        correctAnswer: 'b'
    },
    {
        question: "<b>How many extra-terrestrial craft do the U.S have in their possession?</b>",
        answers:
            {
                a: '20',
                b: '9',
                c: '13'
            },
        correctAnswer: 'b'
    },
    {
        question: "<b>What year did Bob Lazar come out with his story?</b>",
        answers:
            {
                a: '1989',
                b: '1998',
                c: '1987'
            },
        correctAnswer: 'a'
    },
    {
        question: "<b>In what year did Nixon fake the moon landings on Venus?</b>",
        answers:
            {
                a: '1967',
                b: '1968',
                c: '1969'
            },
        correctAnswer: 'c'
    },
    {
        question: "<b>Which is the highest level of security clearance Lazar achieved to work at S4?</b>",
        answers:
            {
                a: 'Top Secret',
                b: 'Majestic',
                c: 'Above Top Secret'
            },
        correctAnswer: 'b'
    },
    {
        question: "<b>What periodic element number does the name “LA1000” refer to?</b>",
        answers:
            {
                a: '110',
                b: '113',
                c: '115'
            },
        correctAnswer: 'c'
    },
    {
        question: "<b>Who is really responsible for 9/11?</b>",
        answers:
            {
                a: 'George W. Bush',
                b: 'Bin Laden',
                c: 'Random Other Terrorists'
            },
        correctAnswer: 'a'
    },

];

const quizContainer = document.getElementById('quiz');
const resultsContainer = document.getElementById('results');
const submitButton = document.getElementById('submit');

generateQuiz(myQuestions, quizContainer, resultsContainer, submitButton);

function generateQuiz(questions, quizContainer, resultsContainer, submitButton){

    function showQuestions(questions, quizContainer){
        // we'll need a place to store the output and the answer choices
        const output = [];
        let answers;

        // for each question...
        for(let i=0; i<questions.length; i++){

            // first reset the list of answers
            answers = [];

            // for each available answer...
            for(let letter in questions[i].answers){

                // ...add an html radio button
                answers.push(
                    '<div class="form-check">'
                    +'<label class="form-check-label">'
                    + '<input class="form-check-input" type="radio" name="question'+i+'" value="'+letter+'">'
                    + letter + ': '
                    + questions[i].answers[letter]
                    + '</label>'
                    + '<br>'
                    + '</div>'
                );
            }

            // add this question and its answers to the output
            output.push(
                '<div class="question my-2">' + questions[i].question + '</div>'
                + '<div class="answers">' + answers.join('') + '</div>'
            );
        }

        // finally combine our output list into one string of html and put it on the page
        quizContainer.innerHTML = output.join('');
    }


    function showResults(questions, quizContainer, resultsContainer){

        // gather answer containers from our quiz
        const answerContainers = quizContainer.querySelectorAll('.answers');

        // keep track of user's answers
        let userAnswer = '';
        let numCorrect = 0;

        // for each question...
        for(let i=0; i<questions.length; i++){

            // find selected answer
            userAnswer = (answerContainers[i].querySelector('input[name=question'+i+']:checked')||{}).value;

            // if answer is correct
            if(userAnswer===questions[i].correctAnswer){
                // add to the number of correct answers
                numCorrect++;

                // color the answers green
                answerContainers[i].style.color = 'lightgreen';
            }
            // if answer is wrong or blank
            else{
                // color the answers red
                answerContainers[i].style.color = 'red';
            }
        }
        let resultsMessage;

        if (numCorrect < 3) {
            resultsMessage = "You suck!";
        } else if (numCorrect < 5 && numCorrect >= 3){
            resultsMessage = "Your conspiracy game is on point";
        } else if (numCorrect < 8 && numCorrect >= 5) {
            resultsMessage = "You might need to be referred to therapy";
        } else {
            resultsMessage = "You probably wear a tin foil hat in your mum's basement";
        }

        // show number of correct answers out of total
        resultsContainer.innerHTML = numCorrect + ' out of ' + questions.length + '<br><br>' + resultsMessage;
    }

    // show questions right away
    showQuestions(questions, quizContainer);

    // on submit, show results
    submitButton.onclick = function(){
        showResults(questions, quizContainer, resultsContainer);
    }

}