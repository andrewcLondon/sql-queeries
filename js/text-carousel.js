$(document).ready(function () {
    $('#statement1').click(function() {
        $(this).fadeOut("slow", function () {
            $(this).empty().append("Ugh it's raining... and there's nothing I can do about it").fadeIn();
        });
    });

    $("#statement2").click(function() {
        $(this).fadeOut("slow", function () {
            $(this).empty().append("He didn't understand what I meant because it wasn't explained very well").fadeIn();
        });
    });

    $("#statement3").click(function() {
        $(this).fadeOut("slow", function () {
            $(this).empty().append("I don't know how to answer the question, I need to learn more about it").fadeIn();
        });
    });
});
